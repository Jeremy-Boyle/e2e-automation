set -x
WORKING_DIR=$(dirname "$0")
pull_secret=$WORKING_DIR/../common/pull-secret.yaml

helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update

kubectl apply -f $WORKING_DIR/kubeapps-dependencies.yaml
ytt -f $WORKING_DIR/kubeapps-helm-values.yaml -f $1 | helm upgrade --install kubeapps bitnami/kubeapps -n kubeapps -f- 


