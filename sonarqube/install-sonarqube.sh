set -x
WORKING_DIR=$(dirname "$0")
pull_secret=$WORKING_DIR/../common/pull-secret.yaml

helm repo add oteemo https://oteemo.github.io/charts
helm repo update

ytt -f $WORKING_DIR/sonar-helm-values.yaml -f $1 | helm upgrade --install sonarqube oteemo/sonarqube -n sonarqube -f- > $WORKING_DIR/chart.yaml

