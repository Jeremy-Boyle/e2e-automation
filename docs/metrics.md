## Config notes

### Add a datasource to grafana. This is the URL:

prometheus-operated.prometheus.svc.cluster.local:9090

## Node Exporter

Add an inboud rule for port 9100 to the SG named node-<cluster name>.  This exposes the metrics endpoint for all the nodes.  This can be done through Cluster API and that would be the right way to do it long term.


